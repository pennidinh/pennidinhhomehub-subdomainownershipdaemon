const NodeRSA = require('node-rsa');
const fs = require('fs');
const cp = require('child_process');
var request = require('request');
const safeJsonStringify = require('safe-json-stringify');
const redis = process.env.REDIS_ENDPOINT ? require("redis").createClient(process.env.REDIS_ENDPOINT) : require("redis").createClient();
redis.on("error", function (err) {
    console.log("Redis Client Error " + err);
});
const uuidv1 = require('uuid/v1');

var internalIpAddress = process.env['INTERNAL_IP_ADDRESS'];
var pennidinhClientId = process.env['PENNIDINH_CLIENT_ID'];

if (pennidinhClientId == null) {
  console.log('PENNIDINH_CLIENT_ID env var must be set!');
  process.exit(1);
}
console.log(pennidinhClientId);

var encryptObject = function(objectToEncrypt, privateKey) {
  return privateKey.encryptPrivate(Buffer.from(JSON.stringify(objectToEncrypt)), 'base64', 'utf8');
}

var sendMessageToPenniDinhCentralServer = function(signed, successCallback, failureCallback) {
  request({
      headers: {
          'x-pennidinh-clientid': pennidinhClientId 
      },
      uri: 'https://subdomainownership.api.pennidinh.com/',
      body: signed,
      method: 'GET'
  }, function (err, res, body) {
      if (err) {
          console.log('err sending request: ' + err);
          if (failureCallback) {
            failureCallback();
          }
          return;
      }

      console.log('res:' + safeJsonStringify(res));
      console.log();
      console.log('body: ' + body);

      if (res.statusCode == 200) {
        if (successCallback) {
          successCallback(body);
        }
      } else {
        if (failureCallback) {
          failureCallback();
        }
      }
  });
}

var handleGetSubdomains = function(privKey, success) {
  var objectToEncrypt = {createdAt: (new Date().getTime() / 1000)};

  var signed = encryptObject(objectToEncrypt, privKey);

  console.log('Fetching subdomains...');
  sendMessageToPenniDinhCentralServer(signed, function(bodyStr) {
    console.log('Returned subdomains: ' + bodyStr);

    const subdomains = JSON.parse(bodyStr);

    if (!Array.isArray(subdomains)) {
      console.log('expected subdomains to be array');
      process.exit(1);
      return;
    }

    if (subdomains.length > 2 || subdomains.length === 0) {
      console.log('Expected 1 or 2 subdomains');
      process.exit(1);
      return;
    }

    let internalName = null;
    let externalName = null;
    for (let subdomain of subdomains) {
      if (typeof subdomain !== 'string') {
        console.log('expected each array item to be a string');
        process.exit(1);
        return;
      }
      if (subdomain.indexOf('internal') >= 0) {
        internalName = subdomain;
      } else {
        externalName = subdomain;
      }
    }

    redis.set('external-subdomain', externalName, function(err) {
      if (err) {
        console.log('unexpected err saving external-subdomain to redis');
        process.exit(1);
      }

      if (internalName) {
        redis.set('internal-subdomain', internalName, function(err) {
          if (err) {
            console.log('unexpected err saving external-subdomain to redis');
            process.exit(1);
          }

          console.log('redis updates complete! (external and internal subdomains)');
          success();
        });
      } else {
        console.log('redis updates complete! (no internal subdomain)');
        success();
      }
    });
  }, function() {
    console.log('fatal fetching pennidinh subdomains');
    process.exit(1);
  });
}

var handleCert = function(privKey, iteration) {
  if (iteration > 10) {
    console.log('cert retries exhaused');
    return;
  }

  redis.get('internal-subdomain', function(err, internalDomainName) {
    if (err) {
      console.log('error fetching internal domain name from redis');
      setTimeout(function() {
        handleCert(privKey, iteration + 1);
      }, 1000 * 10);
      return;
    }

    redis.get('external-subdomain', function(err, externalDomainName) {
      if (err) {
        console.log('error fetching external domain name from redis');
        setTimeout(function() {
          handleCert(privKey, iteration + 1);
        }, 1000 * 10);
        return;
      }

      var objectToEncrypt = {createdAt: (new Date().getTime() / 1000), subdomains: [externalDomainName, internalDomainName]};

      var signed = encryptObject(objectToEncrypt, privKey);

      console.log('Fetching privkey and fullchain from central pennidinh server... (this will block if files not found)');
      sendMessageToPenniDinhCentralServer(signed, function(bodyStr) {
        var body = JSON.parse(bodyStr);

        console.log("Fetced full chain and private key returned");

        fs.readFile('/certs/privkey.pem', 'utf-8', function(err, existingPrivKey) {
            fs.readFile('/certs/fullchain.pem', 'utf-8', function(err, existingFullChain) {
                const handleFullChain = function(needsRestart) {
                  if (existingFullChain !== body.fullChain) {
                      console.log('Full chain has changed. Persiting new one!');
                  } else {
                      console.log('Full chain matches persisted one');

                      if (!needsRestart) {
                          return;
                      }
                  }
                  
                  fs.writeFile('/certs/fullchain.pem', body.fullChain, function(err) {
                    if (err) {
                      console.log('error while persisting fullchain: ' + err);
                      setTimeout(function() {
                        handleCert(privKey, iteration + 1);
                      }, 1000 * 10);
                      return;
                    }
        
                    console.log('successfully fullchain');
        
                    redis.set("hard-restart-apache", uuidv1(), function() {
                        console.log('Successfully notified redis to restart apache');
                    });
                  });
                }

                if (existingPrivKey !== body.privKey) {
                   console.log('Private key has changed. Persisting new one!');
                   fs.writeFile('/certs/privkey.pem', body.privKey, function(err) {
                     if (err) {
                       console.log('error while persisting private key: ' + err);
                       setTimeout(function() {
                         handleCert(privKey, iteration + 1);
                       }, 1000 * 10);
                       return;
                     }

                     console.log('successfully private key');
   
                     handleFullChain(true);
                   });
                } else {
                   console.log('Private key matches persisted one');

                   handleFullChain(false);
                }
            });
        });
      }, function() {
        setTimeout(function() { handleCert(privKey, iteration + 1) }, 120 * 1000 * (iteration + 1)); // retry every 2 minutes * iteration number
      });
    });
  });
}

var handleDynamicDns = function(privKey, iteration) {
  if (iteration > 3) {
    console.log('reached last dynamic dns retry iteration');
    return;
  }

  console.log('Invoking dynamic DNS...');

  redis.get('internal-subdomain', function(err, internalDomainName) {
    if (err) {
      console.log('error fetching internal domain name from redis');
      setTimeout(function() {
        handleDynamicDns(privKey, iteration + 1);
      }, 1000 * 10);
      return;
    }

    redis.get('external-subdomain', function(err, externalDomainName) {
      if (err) {
        console.log('error fetching external domain name from redis');
        setTimeout(function() {
          handleDynamicDns(privKey, iteration + 1);
        }, 1000 * 10);
        return;
      }

      var dnsSettings = {};
      dnsSettings[externalDomainName] = 'PUBLIC_IP';
      if (internalIpAddress) {
        dnsSettings[internalDomainName] = internalIpAddress;
      }
     
      var objectToEncrypt = {createdAt: (new Date().getTime() / 1000), subdomains: [externalDomainName, internalDomainName], dnsSettings: dnsSettings};
    
      var signed = encryptObject(objectToEncrypt, privKey);
    
      console.log('Requesting central pennidinh server to verify (and update if needed) internal and external domain names...');
      sendMessageToPenniDinhCentralServer(signed, function() { console.log('success response from dyndns'); }, function() {
        setTimeout(function() {
          handleDynamicDns(privKey, iteration + 1);
        }, 1000 * 10);
      });
    });
  });
}

var startDaemon = function() {
  fs.readFile('/keys/private.pem', 'utf-8', function(err, contents) {
    if (err) {
      console.log('error while reading private key: ' + err);
      process.exit(1);
      return;
    }

    var privateKey = new NodeRSA(contents);

    setInterval(function() {
      handleDynamicDns(privateKey, 0);
    }, 60 * 60 * 1000); // every hour
    setInterval(function() {
      handleCert(privateKey, 0); 
    }, 24 * 60 * 60 * 1000); // every day

    handleGetSubdomains(privateKey, function() {
      handleDynamicDns(privateKey, 0);
      handleCert(privateKey, 0); 
    });
  });
}

var publicKeyExists = function() {
  console.log('Public key exists!');

  fs.readFile('/keys/public.pem', 'utf-8', function(err, contents) {
    if (err) {
      console.log('error reading public key contents: ' + err);
      return;
    }

    console.log('Public Key: \n' + contents);

    request({
      uri: 'https://clientidclaim.api.pennidinh.com/',
      body: JSON.stringify({clientId: pennidinhClientId, publicKey: contents}),
      method: 'POST'
    }, function (err, res, body) {
      if (err) {
        console.log('err uploading public key: ' + err);
        process.exit(1);
      }
  
      console.log('res:' + safeJsonStringify(res));
      console.log();
      console.log('body: ' + body);
  
      if (res.statusCode >= 500) {
        console.log('err ' + res.statusCode + ' uploading public key');
        process.exit(1);
      } 

      startDaemon();
    });
  });
}

var privateKeyExists = function() {
 console.log('Private key exists!');

 fs.stat('/keys/public.pem', function(err) {
    if (err) {
      console.log('public key does not exist. creating public key...');

      cp.exec('openssl rsa -in /keys/private.pem -outform PEM -pubout -out /keys/public.pem', {}, function(err, stdout, stderr) {
        if (err) {
          console.log('error generating public key: ' + err);
          return;
        }

        publicKeyExists();
      });
    } else {
      publicKeyExists();
    }
  });
};

fs.stat('/keys/private.pem', function(err) {
  if (err) {
    console.log('private key does not exist. creating private key...');
    cp.exec('openssl genrsa -out /keys/private.pem 2048', {}, function(err, stdout, stderr) {
      if (err) {
        console.log('error generating private key: ' + err);
        return;
      }

      privateKeyExists();
    });
  } else {
    privateKeyExists();
  }
});
