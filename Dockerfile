FROM node:13.2.0

COPY ./package.json /app/package.json

RUN cd /app && npm install

RUN mkdir /keys
RUN mkdir /certs

COPY ./ /app/

CMD node /app/daemon.js
